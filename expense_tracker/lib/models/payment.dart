class Payment {
  
  final String paymentId;
  final String paymentName;
  final String paymentAccount;
  final String paymentNote;
  final String paymentIcon;

  Payment(
      {this.paymentId,
      this.paymentName,
      this.paymentAccount,
      this.paymentNote,
      this.paymentIcon,
      });

     
  
}