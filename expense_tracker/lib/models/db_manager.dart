import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DbManager {
  static final _databaseName = "live_expense.db";
  static final _databaseVersion = 1;

  // make this a singleton class
  DbManager._privateConstructor();
  static final DbManager instance = DbManager._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute(
      "PRAGMA foreign_keys = ON;",
    );

    await db.execute(
      "CREATE TABLE " + pinTable + "(pinId INTEGER PRIMARY KEY, pinText TEXT)",
    );

    await db.execute(
      "CREATE TABLE " +
          groupTable +
          "(groupId INTEGER PRIMARY KEY, groupName TEXT, groupAccount TEXT,  groupNote TEXT, groupIcon TEXT)",
    );

    await db.execute(
      "CREATE TABLE " +
          projectTable +
          "(projectId INTEGER PRIMARY KEY, projectName TEXT, projectAccount TEXT,  projectNote TEXT, projectIcon TEXT)",
    );

    await db.execute(
      "CREATE TABLE " +
          currencyTable +
          "(currencyId INTEGER PRIMARY KEY, currencyName TEXT, currencyCode TEXT,  currencyNote TEXT, currencyIsDefault INTEGER)",
    );

    await db.execute(
      "CREATE TABLE " +
          paymentTable +
          "(paymentId INTEGER PRIMARY KEY, paymentName TEXT, paymentAccount TEXT, paymentNote TEXT, paymentIcon TEXT)",
    );

    await db.execute(
      "CREATE TABLE " +
          transactionTable +
          "(transactionId INTEGER PRIMARY KEY, transactionType INTEGER, transactionTime DATETIME, " +
          "transactionAmount DOUBLE, transactionGroupId INTEGER, transactionProjectId INTEGER, transactionPaymentId INTEGER, " +
          "transactionAttachment TEXT, transactionNote TEXT, " +
          "FOREIGN KEY(transactionGroupId) REFERENCES " +
          groupTable +
          "(groupId) ON DELETE RESTRICT, " +
          "FOREIGN KEY(transactionProjectId) REFERENCES " +
          projectTable +
          "(projectId) ON DELETE RESTRICT, " +
          "FOREIGN KEY(transactionPaymentId) REFERENCES " +
          paymentTable +
          "(paymentId) ON DELETE RESTRICT" +
          ")",
    );
  }

  ////pin table

  static final pinTable = 'PinTBL';

  Future<int> insertPin(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(pinTable, row);
  }

  Future<List<Map<String, dynamic>>> getPin(String tryPin) async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.query(pinTable);

    if (rows.length == 0) {
      await insertPin({'pinText': tryPin});
      rows = await db.query(pinTable);
    }

    return rows;
  }

  Future<int> deletePin() async {
    Database db = await instance.database;

    return await db.delete(pinTable);
  }

  //end pin table

  ////group table

  static final groupTable = 'GroupTBL';

  Future<int> insertGroup(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(groupTable, row);
  }

  Future<List<Map<String, dynamic>>> getAllGroups({String searchText}) async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.query(groupTable, where: ' groupName Like ? ', whereArgs: ['%$searchText%']);
    return rows;
  }

  Future<int> updateGroup(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int groupId = int.parse(row['groupId']);
    return await db
        .update(groupTable, row, where: 'groupId = ?', whereArgs: [groupId]);
  }

  Future<int> deleteGroup(int groupId) async {
    Database db = await instance.database;

    List<Map<String, dynamic>> rows = await db.rawQuery("select * from " +
        transactionTable +
        " where transactionGroupId = " +
        groupId.toString() +
        " limit 0,1");

    if (rows.length > 0) {
      return -1;
    }

    return await db
        .delete(groupTable, where: 'groupId = ?', whereArgs: [groupId]);
  }

  //end group table

  ////project table

  static final projectTable = 'ProjectTBL';

  Future<int> insertProject(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(projectTable, row);
  }

  Future<List<Map<String, dynamic>>> getAllProjects({String searchText}) async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.query(projectTable, where: ' projectName Like ?', whereArgs: ['%$searchText%']);
    return rows;
  }

  Future<int> updateProject(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int projectId = int.parse(row['projectId']);
    return await db.update(projectTable, row,
        where: 'projectId = ?', whereArgs: [projectId]);
  }

  Future<int> deleteProject(int projectId) async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.rawQuery("select * from " +
        transactionTable +
        " where transactionProjectId = " +
        projectId.toString() +
        " limit 0,1");

    if (rows.length > 0) {
      return -1;
    }
    return await db
        .delete(projectTable, where: 'projectId = ?', whereArgs: [projectId]);
  }

  //end project table

  ////currency table

  static final currencyTable = 'CurrencyTBL';

  Future<int> insertCurrency(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(currencyTable, row);
  }

  Future<List<Map<String, dynamic>>> getAllCurrencies() async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.query(currencyTable);
    return rows;
  }

  Future<int> updateCurrency(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int currencyId = int.parse(row['currencyId']);
    return await db.update(currencyTable, row,
        where: 'currencyId = ?', whereArgs: [currencyId]);
  }

  Future<int> deleteCurrency(int currencyId) async {
    Database db = await instance.database;
    return await db.delete(currencyTable,
        where: 'currencyId = ?', whereArgs: [currencyId]);
  }

  //end currency table

  ////payment table

  static final paymentTable = 'PaymentTBL';

  Future<int> insertPayment(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(paymentTable, row);
  }

  Future<List<Map<String, dynamic>>> getAllPayments() async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.query(paymentTable);
    return rows;
  }

  Future<int> updatePayment(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int paymentId = int.parse(row['paymentId']);
    return await db.update(paymentTable, row,
        where: 'paymentId = ?', whereArgs: [paymentId]);
  }

  Future<int> deletePayment(int paymentId) async {
    Database db = await instance.database;

    List<Map<String, dynamic>> rows = await db.rawQuery("select * from " +
        transactionTable +
        " where transactionPaymentId = " +
        paymentId.toString() +
        " limit 0,1");

    if (rows.length > 0) {
      return -1;
    }

    return await db
        .delete(paymentTable, where: 'paymentId = ?', whereArgs: [paymentId]);
  }

  //end project table

  ////transaction table

  static final transactionTable = 'TransactionTBL';

  Future<int> insertTransaction(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(transactionTable, row);
  }

  Future<List<Map<String, dynamic>>> getTransaction(int id) async {
    Database db = await instance.database;

    List<Map<String, dynamic>> rows = await db.rawQuery('select * from ' +
        transactionTable +
        ' where transactionId = ' +
        id.toString());

    return rows;
  }

  Future<int> updateTransaction(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int transactionId = int.parse(row['transactionId']);
    return await db.update(transactionTable, row,
        where: 'transactionId = ?', whereArgs: [transactionId]);
  }

  Future<int> deleteTransaction(int transactionId) async {
    Database db = await instance.database;
    return await db.delete(transactionTable,
        where: 'transactionId = ?', whereArgs: [transactionId]);
  }

  //end transaction table

  ///dashboard query

  Future<List<Map<String, dynamic>>> getOptimizedTransactionList(
      {String groupId,
      String projectId,
      String paymentId,
      String from,
      String to,
      String limit}) async {
    Database db = await instance.database;

    String query = "SELECT " +
        "groupId, groupName, groupIcon, projectId, projectName, projectIcon,  paymentId, paymentName, paymentIcon, " +
        "transactionId, transactionAmount, transactionGroupId, transactionProjectId, transactionPaymentId, transactionTime, transactionType, transactionNote " +
        " FROM " +
        transactionTable +
        ", " +
        groupTable +
        ", " +
        projectTable +
        ", " +
        paymentTable +
        " WHERE " +
        transactionTable +
        ".transactionGroupId = " +
        groupTable +
        ".groupId and " +
        transactionTable +
        ".transactionProjectId = " +
        projectTable +
        ".projectId and " +
        transactionTable +
        ".transactionPaymentId = " +
        paymentTable +
        ".paymentId ";

    if (groupId != null) {
      query = query + " and transactionGroupId = " + groupId;
    }

    if (projectId != null) {
      query = query + " and transactionProjectId = " + projectId;
    }

    if (paymentId != null) {
      query = query + " and transactionPaymentId = " + paymentId;
    }

    if (from != null && to != null) {
      query = query +
          " and transactionTime between '" +
          from +
          "' and '" +
          to +
          "' ";
    }

    query += " order by " + transactionTable + ".transactionTime desc";

    if (limit != null) {
      query += " limit 0, " + limit;
    }

    List<Map<String, dynamic>> rows = await db.rawQuery(query);

    return rows;
  }

  Future<List<Map<String, dynamic>>> getCurrentProject() async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.rawQuery("select * from " +
        projectTable +
        " where " +
        "projectId in(select transactionProjectId from " +
        transactionTable +
        " order by transactionId desc limit 0,1)");

    if (rows.length == 0) {
      rows = await db.rawQuery("select * from " + projectTable + " limit 0,1");
    }

    if (rows.length == 0) {
      await insertProject({'projectName': 'Main'});

      rows = await db.rawQuery("select * from " + projectTable + " limit 0,1");
    }

    List<Map<String, dynamic>> balance =
        await getBalance(projectId: rows[0]['projectId'].toString());

    List<Map<String, dynamic>> result = [
      {
        'projectId': rows[0]['projectId'],
        'projectName': rows[0]['projectName'],
        'projectAccount': rows[0]['projectAccount'],
        'projectNote': rows[0]['projectNote'],
        'projectIcon': rows[0]['projectIcon'],
        'projectBalance': balance[0]['balance'],
      }
    ];

    return result;
  }

  Future<List<Map<String, dynamic>>> getBalance(
      {String groupId,
      String projectId,
      String paymentId,
      String from,
      String to}) async {
    Database db = await instance.database;

    String query = "select sum(case " +
        "when transactionType != 0 then transactionAmount " +
        "Else  -1 * transactionAmount " +
        "end) as balance from " +
        transactionTable +
        " where 1=1 ";

    if (groupId != null) {
      query = query + " and transactionGroupId = " + groupId;
    }

    if (projectId != null) {
      query = query + " and transactionProjectId = " + projectId;
    }

    if (paymentId != null) {
      query = query + " and transactionPaymentId = " + paymentId;
    }

    if (from != null && to != null) {
      query = query + " and transactionTime between '" + from + "' and '" + to + "'";
    }

    List<Map<String, dynamic>> rows = await db.rawQuery(query);

    return rows;
  }

  Future<List<Map<String, dynamic>>> getCurrency() async {
    Database db = await instance.database;

    String query = "select * from " +
        currencyTable +
        " where currencyIsDefault = 1 limit 0, 1";

    List<Map<String, dynamic>> rows = await db.rawQuery(query);

    if (rows.length == 0) {
      String query = "select * from " + currencyTable + " limit 0, 1";

       rows = await db.rawQuery(query);
    }

    return rows;
  }

  ///end dashboard query

}
