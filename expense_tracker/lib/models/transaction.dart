class Transaction {
  
  final String transactionId;
  final int transactionType;
  final DateTime transactionTime;
  final double transactionAmount;
  final int transactionGroupId;
  final int transactionProjectId;
  final int transactionPaymentId;
  final String transactionAttachment;
  final String transactionNote;

  Transaction(
      {this.transactionId,
      this.transactionType,
      this.transactionTime,
      this.transactionAmount,
      this.transactionGroupId,
      this.transactionProjectId,
      this.transactionPaymentId,
      this.transactionAttachment,
      this.transactionNote,
      });

     
  
}