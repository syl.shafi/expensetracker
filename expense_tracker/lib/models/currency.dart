class Currency {
  final String currencyId;
  final String currencyName;
  final String currencyCode;
  final String currencyNote;
  final int currencyIsDefault;

  Currency({
    this.currencyId,
    this.currencyName,
    this.currencyCode,
    this.currencyNote,
    this.currencyIsDefault,
  });
}
