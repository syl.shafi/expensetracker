class Group {
  
  final String groupId;
  final String groupName;
  final String groupAccount;
  final String groupNote;
  final String groupIcon;

  Group(
      {this.groupId,
      this.groupName,
      this.groupAccount,
      this.groupNote,
      this.groupIcon,
      });

     
  
}