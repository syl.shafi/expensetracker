import 'dart:convert';
import 'package:flutter/material.dart';
import 'bottom_menu.dart';
import '../models/db_manager.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  final dbHelper = DbManager.instance;

  String currDate;
  int currTransactionId;
  BottomMenu bottomMenu = new BottomMenu(currentIndex: 1);
  String currencyCode;
  double balance = 0.00;

  String projectId;
  DateTime from = new DateTime(
      DateTime.now().year, DateTime.now().month, DateTime.now().day);
  DateTime to = new DateTime(
      DateTime.now().year, DateTime.now().month, DateTime.now().day);

  Future<List<Map<String, dynamic>>> getData(
      {String from, String to, String projectId}) async {
    List<Map<String, dynamic>> rows = projectId != null
        ? await dbHelper.getOptimizedTransactionList(
            from: from, to: to, projectId: projectId)
        : await dbHelper.getOptimizedTransactionList(
            from: from, to: to, projectId: null);

    return rows;
  }

  Future<void> getCurrencyCode() async {
    List<Map<String, dynamic>> rows = await dbHelper.getCurrency();

    setState(() {
      if (rows.length == 0) {
        currencyCode = 'US\$';
      } else {
        currencyCode = rows[0]['currencyCode'];
      }
    });
  }

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Future<List<Map<String, dynamic>>> getProjects() async {
    return await dbHelper.getAllProjects(searchText: '');
  }

  void _delete(int id, BuildContext context) async {
    final rowsDeleted = await dbHelper.deleteTransaction(id);
    //print('deleted $rowsDeleted row(s): row $id');
    Navigator.of(context).pushReplacementNamed('/history');
  }

  void openInsertTransactionScreen(
      BuildContext context, int transactionType, int transactionProjectId) {
    Navigator.of(context).pushReplacementNamed('/insertTransaction',
        arguments: <String, dynamic>{
          "transactionType": transactionType,
          "transactionProjectId": transactionProjectId,
        });
  }

  Future<List<Map<String, dynamic>>> getBalance(
      String projectId, String fromDate, String toDate) async {
    List<Map<String, dynamic>> rows = projectId != null
        ? await dbHelper.getBalance(
            projectId: projectId, from: fromDate, to: toDate)
        : await dbHelper.getBalance(
            projectId: null, from: fromDate, to: toDate);
    return rows;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrencyCode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('History')),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: () async {
                    DateTime tempDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime.utc(1800, 1, 1),
                      lastDate: DateTime.now().add(Duration(days: 30)),
                    );

                    setState(() {
                      from = tempDate != null
                          ? tempDate
                          : new DateTime(DateTime.now().year,
                              DateTime.now().month, DateTime.now().day);
                    });
                  },
                  child: Text('From : ' + from.toString().substring(0, 10),
                      style: TextStyle(fontSize: 20)),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: () async {
                    DateTime tempDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime.utc(1800, 1, 1),
                      lastDate: DateTime.now().add(Duration(days: 30)),
                    );

                    setState(() {
                      to = tempDate != null
                          ? tempDate
                          : new DateTime(DateTime.now().year,
                              DateTime.now().month, DateTime.now().day);
                    });
                  },
                  child: Text('To : ' + to.toString().substring(0, 10),
                      style: TextStyle(fontSize: 20)),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                alignment: Alignment.topRight,
                color: Colors.grey[300],
                child: FutureBuilder<List<Map<String, dynamic>>>(
                    future: getProjects(),
                    builder: (context,
                        AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
                      if (dataList.hasData) {
                        return new DropdownButtonFormField<int>(
                          value: projectId != null
                              ? int.parse("" + projectId)
                              : null,
                          isExpanded: true,
                          items: [
                            new DropdownMenuItem(
                              value: null,
                              child: Text('All'),
                            )
                          ]..addAll(
                              dataList.data
                                  .map(
                                    (item) => new DropdownMenuItem(
                                        value: int.parse(
                                            item['projectId'].toString()),
                                        child: Text(item['projectName'])),
                                  )
                                  .toList(),
                            ),
                          onChanged: (value) {
                            setState(() {
                              projectId =
                                  value == null ? null : value.toString();
                            });
                          },
                        );
                      } else {
                        return new DropdownButtonFormField<int>(
                          value: 1,
                          isExpanded: true,
                          items: [],
                          onChanged: (value) {},
                        );
                      }
                    }),
              ),
              FutureBuilder<List<Map<String, dynamic>>>(
                  future: getBalance(
                    projectId,
                    from.toString().substring(0, 23),
                    to.add(new Duration(days: 1)).toString().substring(0, 23),
                  ),
                  builder: (context,
                      AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
                    if (dataList.hasData) {
                      balance = dataList.data[0]['balance'] == null
                          ? 0
                          : dataList.data[0]['balance'];

                      return Container(
                        width: MediaQuery.of(context).size.width,
                        height: 20,
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        child: Text(
                          '$currencyCode ${balance.toStringAsFixed(2)}',
                          textAlign: TextAlign.center,
                        ),
                      );
                    } else {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        height: 20,
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        child: Text(
                          '$currencyCode ${balance.toStringAsFixed(2)}',
                          textAlign: TextAlign.center,
                        ),
                      );
                    }
                  }),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.5,
                child: FutureBuilder<List<Map<String, dynamic>>>(
                    future: getData(
                        from: from.toString().substring(0, 23),
                        to: to
                            .add(new Duration(days: 1))
                            .toString()
                            .substring(0, 23),
                        projectId: projectId),
                    builder: (context,
                        AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
                      if (dataList.hasData) {
                        return ListView.builder(
                          itemCount: dataList.data.length,
                          itemBuilder: (BuildContext ctx, int index) {
                            if (currDate == null ||
                                currDate !=
                                    dataList.data[index]['transactionTime']
                                        .toString()
                                        .substring(0, 10) ||
                                currTransactionId ==
                                    dataList.data[index]['transactionId']) {
                              currTransactionId =
                                  dataList.data[index]['transactionId'];

                              currDate = dataList.data[index]['transactionTime']
                                  .toString()
                                  .substring(0, 10);

                              return Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                      color: Colors.grey,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      currDate,
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.right,
                                    ),
                                  ),
                                  Dismissible(
                                      background: Container(
                                        color: Colors.red,
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                        ),
                                      ),
                                      key: Key(UniqueKey().toString()),
                                      confirmDismiss:
                                          (DismissDirection direction) async {
                                        return await showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: const Text("Confirm"),
                                              content: const Text(
                                                  "Are you sure you wish to delete this?"),
                                              actions: <Widget>[
                                                FlatButton(
                                                    onPressed: () =>
                                                        Navigator.of(context)
                                                            .pop(true),
                                                    child:
                                                        const Text("DELETE")),
                                                FlatButton(
                                                  onPressed: () =>
                                                      Navigator.of(context)
                                                          .pop(false),
                                                  child: const Text("CANCEL"),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                      onDismissed: (direction) {
                                        _delete(
                                            dataList.data[index]
                                                ['transactionId'],
                                            context);
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                              bottom: new BorderSide(
                                                  color: Colors.grey)),
                                        ),
                                        child: ListTile(
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 20.0, vertical: 10.0),
                                          leading: Container(
                                            decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              border: Border.all(
                                                  width: 2.0,
                                                  color: Colors.grey),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8.0)),
                                            ),
                                            child: dataList.data[index]
                                                        ['groupIcon'] !=
                                                    null
                                                ? Icon(
                                                    fromJSONStringToIconData(
                                                        dataList.data[index]
                                                            ['groupIcon']),
                                                    size: 40,
                                                  )
                                                : Icon(
                                                    Icons.poll,
                                                    size: 40,
                                                  ),
                                          ),
                                          title: Text(dataList.data[index]
                                              ['groupName']),
                                          subtitle: dataList.data[index]
                                                      ['transactionType'] !=
                                                  0
                                              ? Text(
                                                  '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.green),
                                                )
                                              : Text(
                                                  '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.red)),
                                          trailing: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                3,
                                            child: IconButton(
                                              icon: Icon(
                                                Icons.arrow_right,
                                                size: 40,
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                          onTap: () {
                                            Navigator.of(context)
                                                .pushReplacementNamed(
                                                    '/insertTransaction',
                                                    arguments: <String,
                                                        dynamic>{
                                                  "transactionId":
                                                      dataList.data[index]
                                                          ['transactionId'],
                                                  "transactionType":
                                                      dataList.data[index]
                                                          ['transactionType'],
                                                  "transactionTime":
                                                      dataList.data[index]
                                                          ['transactionTime'],
                                                  "transactionAmount":
                                                      dataList.data[index]
                                                          ['transactionAmount'],
                                                  "transactionGroupId": dataList
                                                          .data[index]
                                                      ['transactionGroupId'],
                                                  "transactionProjectId": dataList
                                                          .data[index]
                                                      ['transactionProjectId'],
                                                  "transactionPaymentId": dataList
                                                          .data[index]
                                                      ['transactionPaymentId'],
                                                  "transactionAttachment": dataList
                                                          .data[index]
                                                      ['transactionAttachment'],
                                                  "transactionNote":
                                                      dataList.data[index]
                                                          ['transactionNote'],
                                                  "returnRoute": '/history'
                                                });
                                          },
                                        ),
                                      )),
                                  dataList.data.length == index + 1
                                      ? ListTile()
                                      : SizedBox()
                                ],
                              );
                            } else {
                              return Column(children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                        bottom:
                                            new BorderSide(color: Colors.grey)),
                                  ),
                                  child: Dismissible(
                                    background: Container(
                                      color: Colors.red,
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.white,
                                      ),
                                    ),
                                    key: Key(index.toString()),
                                    confirmDismiss:
                                        (DismissDirection direction) async {
                                      return await showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: const Text("Confirm"),
                                            content: const Text(
                                                "Are you sure you wish to delete this?"),
                                            actions: <Widget>[
                                              FlatButton(
                                                  onPressed: () =>
                                                      Navigator.of(context)
                                                          .pop(true),
                                                  child: const Text("DELETE")),
                                              FlatButton(
                                                onPressed: () =>
                                                    Navigator.of(context)
                                                        .pop(false),
                                                child: const Text("CANCEL"),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                    onDismissed: (direction) {
                                      _delete(
                                          dataList.data[index]['transactionId'],
                                          context);
                                    },
                                    child: ListTile(
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 20.0, vertical: 10.0),
                                      leading: Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(
                                              width: 2.0, color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8.0)),
                                        ),
                                        child: dataList.data[index]
                                                    ['groupIcon'] !=
                                                null
                                            ? Icon(
                                                fromJSONStringToIconData(
                                                    dataList.data[index]
                                                        ['groupIcon']),
                                                size: 40,
                                              )
                                            : Icon(
                                                Icons.poll,
                                                size: 40,
                                              ),
                                      ),
                                      title: Text(
                                          dataList.data[index]['groupName']),
                                      subtitle: dataList.data[index]
                                                  ['transactionType'] !=
                                              0
                                          ? Text(
                                              '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                              style: TextStyle(
                                                  color: Colors.green),
                                            )
                                          : Text(
                                              '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                              style:
                                                  TextStyle(color: Colors.red)),
                                      trailing: Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.arrow_right,
                                            size: 40,
                                          ),
                                          onPressed: () {},
                                        ),
                                      ),
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushReplacementNamed(
                                                '/insertTransaction',
                                                arguments: <String, dynamic>{
                                              "transactionId": dataList
                                                  .data[index]['transactionId'],
                                              "transactionType":
                                                  dataList.data[index]
                                                      ['transactionType'],
                                              "transactionTime":
                                                  dataList.data[index]
                                                      ['transactionTime'],
                                              "transactionAmount":
                                                  dataList.data[index]
                                                      ['transactionAmount'],
                                              "transactionGroupId":
                                                  dataList.data[index]
                                                      ['transactionGroupId'],
                                              "transactionProjectId":
                                                  dataList.data[index]
                                                      ['transactionProjectId'],
                                              "transactionPaymentId":
                                                  dataList.data[index]
                                                      ['transactionPaymentId'],
                                              "transactionAttachment":
                                                  dataList.data[index]
                                                      ['transactionAttachment'],
                                              "transactionNote":
                                                  dataList.data[index]
                                                      ['transactionNote'],
                                              "returnRoute": '/history'
                                            });
                                      },
                                    ),
                                  ),
                                ),
                                dataList.data.length == index + 1
                                    ? ListTile()
                                    : SizedBox()
                              ]);
                            }
                          },
                        );
                      } else {
                        return CircularProgressIndicator();
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomMenu.getBottomMenu(context),
    );
  }
}
