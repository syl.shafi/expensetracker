import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/db_manager.dart';

class PaymentList {
  static final dbHelper = DbManager.instance;

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Future<List<Map<String, dynamic>>> _getData() async {
    return await dbHelper.getAllPayments();
  }

  Future<int> _delete(int id) async {
    final stat = await dbHelper.deletePayment(id);
    return stat;
  }

  Widget getPaymentList(BuildContext context) {
    return FutureBuilder<List<Map<String, dynamic>>>(
        future: _getData(),
        builder: (context, AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
          if (dataList.hasData) {
            return ListView.builder(
                itemCount: dataList.data.length,
                itemBuilder: (BuildContext ctx, int index) {
                  if (dataList.data[index]['paymentId'] != null) {
                    return Container(
                      decoration: BoxDecoration(
                        border:
                            Border(bottom: new BorderSide(color: Colors.grey)),
                      ),
                      child: Dismissible(
                        background: Container(
                          color: Colors.red,
                          child: Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                        ),
                        key: Key(UniqueKey().toString()),
                        confirmDismiss: (DismissDirection direction) async {
                          return await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text("Confirm"),
                                content: const Text(
                                    "Are you sure you wish to delete this?"),
                                actions: <Widget>[

                                 FlatButton(
                                      onPressed: () async {
                                        int stat = await _delete(
                                            dataList.data[index]['paymentId']);
                                        if (stat == -1) {
                                          return await showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                title: const Text("Error"),
                                                content: const Text(
                                                    "There are Transactions under this item."),
                                                actions: <Widget>[
                                                  FlatButton(
                                                    onPressed: () =>
                                                        Navigator.of(context)
                                                            .pop(false),
                                                    child: const Text("OK"),
                                                  ),
                                                ],
                                              );
                                            },
                                          );
                                        } else {
                                         return Navigator.of(context).pop(true);
                                        }
                                      },
                                      child: const Text("DELETE")),
                                  FlatButton(
                                    onPressed: () =>
                                        Navigator.of(context).pop(false),
                                    child: const Text("CANCEL"),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                        onDismissed: (direction) {
                          _delete(dataList.data[index]['paymentId']);
                        },
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10.0),
                          leading: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border:
                                  Border.all(width: 2.0, color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                            ),
                            child: dataList.data[index]['paymentIcon'] != null
                                ? Icon(
                                    fromJSONStringToIconData(
                                        dataList.data[index]['paymentIcon']),
                                    size: 40,
                                  )
                                : Icon(
                                    Icons.payment,
                                    size: 40,
                                  ),
                          ),
                          title: Text(dataList.data[index]['paymentName']),
                          trailing: IconButton(
                            icon: Icon(
                              Icons.arrow_right,
                              size: 40,
                            ),
                            onPressed: () {},
                          ),
                          onTap: () {
                            Navigator.of(context).pushReplacementNamed(
                                "/createpayment",
                                arguments: <String, dynamic>{
                                  "paymentId": dataList.data[index]
                                      ['paymentId'],
                                  "paymentName": dataList.data[index]
                                      ['paymentName'],
                                  "paymentAccount": dataList.data[index]
                                      ['paymentAccount'],
                                  "paymentNote": dataList.data[index]
                                      ['paymentNote'],
                                  "paymentIcon": dataList.data[index]
                                      ['paymentIcon'],
                                });
                          },
                        ),
                      ),
                    );
                  } else {
                    return Center(
                      child: SizedBox()
                    );
                  }
                });
          } else {
            return Center(
              child: SizedBox()
            );
          }
        });
  }
}
