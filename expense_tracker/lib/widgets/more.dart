import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/db_manager.dart';
import './bottom_menu.dart';

class More extends StatelessWidget {
  final dbHelper = DbManager.instance;

  BottomMenu bottomMenu = new BottomMenu(currentIndex: 3);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('More Features'),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                onPressed: () async {
                  SharedPreferences dataPrefs =
                      await SharedPreferences.getInstance();

                  await dataPrefs.setBool('loggedIn', false);

                  Navigator.of(context).pushReplacementNamed('/');
                },
                child: const Text('Log Out', style: TextStyle(fontSize: 20)),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                onPressed: () async {
                  SharedPreferences dataPrefs =
                      await SharedPreferences.getInstance();

                  await dataPrefs.setBool('loggedIn', false);

                  await dbHelper.deletePin();

                  Navigator.of(context).pushReplacementNamed('/');
                },
                child: const Text('Reset Pin', style: TextStyle(fontSize: 20)),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomMenu.getBottomMenu(context),
    );
  }
}
