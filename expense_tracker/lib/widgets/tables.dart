import 'package:flutter/material.dart';
import 'bottom_menu.dart';
import './group_list.dart';
import './project_list.dart';
import './currency_list.dart';
import './payment_list.dart';

class Tables extends StatefulWidget {
  
  final GroupList groupList = new GroupList();
  final ProjectList projectList = new ProjectList();
  final CurrencyList currencyList = new CurrencyList();
  final PaymentList paymentList = new PaymentList();

  @override
  _TablesState createState() => _TablesState();
}

class _TablesState extends State<Tables> {
  BottomMenu bottomMenu = new BottomMenu(currentIndex: 2);

  
  String searchText = '';
  TextEditingController searchTextController = new TextEditingController();

  
  void changeSearchText(String text){
        
        setState(() {
          searchText = text;
        });
         
  }

  void callSetState(){
        
        setState(() {
        });
         
  }

  bool pageShifted = true;

  int menu = 0;

  void changeMenuEvent(int index) {
    setState(() {
      menu = index;
    });
  }

  
   @override
  void initState() {
    super.initState();
    searchTextController.addListener(() {
      changeSearchText(searchTextController.text);
    });
  }

  Widget build(BuildContext context) {

    if(pageShifted){

      if(ModalRoute.of(context).settings.arguments != null){

        final  Map<String, dynamic> data = ModalRoute.of(context).settings.arguments;
        menu = int.parse(data["menu"].toString());

      }

      pageShifted = false;

    }

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.22,
            child: FlatButton(
              padding: EdgeInsets.all(0),
              textColor: menu == 0 ? Colors.black : Colors.white,
              color: menu == 0 ? Colors.white : Theme.of(context).primaryColor,
              onPressed: () {
                changeMenuEvent(0);
              },
              child: Text(
                "Groups",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
              ),
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.white, width: 2, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.22,
            child: FlatButton(
              padding: EdgeInsets.all(0),
              textColor: menu == 1 ? Colors.black : Colors.white,
              color: menu == 1 ? Colors.white : Theme.of(context).primaryColor,
              onPressed: () {
                changeMenuEvent(1);
              },
              child: Text(
                "Projects",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
              ),
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.white, width: 2, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.22,
            child: FlatButton(
              padding: EdgeInsets.all(0),
              textColor: menu == 2 ? Colors.black : Colors.white,
              color: menu == 2 ? Colors.white : Theme.of(context).primaryColor,
              onPressed: () {
                changeMenuEvent(2);
              },
              child: Text(
                "Currencies",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
              ),
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.white, width: 2, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.22,
            child: FlatButton(
              padding: EdgeInsets.all(0),
              textColor: menu == 3 ? Colors.black : Colors.white,
              color: menu == 3 ? Colors.white : Theme.of(context).primaryColor,
              onPressed: () {
                changeMenuEvent(3);
              },
              child: Text(
                "Payment",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
              ),
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.white, width: 2, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.12,
            child: IconButton(
              icon: Icon(
                Icons.add,
                size: 30,
                color: Colors.white,
              ),
              onPressed: () {
                if (menu == 0) {
                  Navigator.of(context).pushReplacementNamed("/creategroup");
                }
                else if  (menu == 1){
                  Navigator.of(context).pushReplacementNamed("/createproject");
                }
                else if  (menu == 2){
                  Navigator.of(context).pushReplacementNamed("/createcurrency");
                }
                else{
                  Navigator.of(context).pushReplacementNamed("/createpayment");
                }
              },
            ),
          )
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.8,
                width: MediaQuery.of(context).size.width,
                child: menu == 0
                    ? widget.groupList.getGroupList(context, searchText: searchText, searchTextController: searchTextController)
                    : menu == 1 ? widget.projectList.getProjectList(context, searchText: searchText, searchTextController: searchTextController  )
                    : menu == 2 ? widget.currencyList.getCurrencyList(context, callSetState)
                    : widget.paymentList.getPaymentList(context),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomMenu.getBottomMenu(context),
    );
  }
}
