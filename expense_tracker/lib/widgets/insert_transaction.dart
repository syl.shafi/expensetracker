import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import '../models/db_manager.dart';

class InsertTransaction extends StatefulWidget {
  final dbHelper = DbManager.instance;

  @override
  _InsertTransactionState createState() => _InsertTransactionState();
}

class _InsertTransactionState extends State<InsertTransaction> {
  final _formKey = GlobalKey<FormState>();

  String transactionId;
  int transactionType;
  DateTime transactionTime = DateTime.now();
  double transactionAmount;
  int transactionGroupId;
  int transactionProjectId;
  int transactionPaymentId;
  String transactionAttachment;
  String transactionNote;
  String returnRoute = '/';
  String currencyCode = '...';

  //File _image;
  final picker = ImagePicker();

  bool pageShifted = true;

  TextEditingController amountController = new TextEditingController();
  TextEditingController noteController = new TextEditingController();

  var amountFocusNode = new FocusNode();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    transactionAttachment = null;

    if (pickedFile != null) {
      //_image = File(pickedFile.path);
      var bytes = await pickedFile.readAsBytes();
      bytes = await FlutterImageCompress.compressWithList(
        bytes,
        minHeight: 320,
        minWidth: 200,
        quality: 96,
        rotate: 0,
      );
      transactionAttachment = base64Encode(bytes);
    } else {
      //print('No image selected.');
    }

    setState(() {});
  }

  Future<void> getCurrencyCode() async {
    List<Map<String, dynamic>> rows = await widget.dbHelper.getCurrency();

    setState(() {
      if (rows.length == 0) {
        currencyCode = 'US\$';
      } else {
        currencyCode = rows[0]['currencyCode'];
      }
    });
  }

  Future<List<Map<String, dynamic>>> getGroups() async {
    return await widget.dbHelper.getAllGroups(searchText: '');
  }

  Future<List<Map<String, dynamic>>> getProjects() async {
    return await widget.dbHelper.getAllProjects(searchText: '');
  }

  Future<List<Map<String, dynamic>>> getPayments() async {
    return await widget.dbHelper.getAllPayments();
  }

  void _insert(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "transactionId": transactionId,
      "transactionType": transactionType,
      "transactionTime": transactionTime.toString().substring(0, 23),
      "transactionAmount": transactionAmount,
      "transactionGroupId": transactionGroupId,
      "transactionProjectId": transactionProjectId,
      "transactionPaymentId": transactionPaymentId,
      "transactionAttachment": transactionAttachment,
      "transactionNote": transactionNote,
    };
    final id = await widget.dbHelper.insertTransaction(row);
    //print('inserted row id: $id');
    Navigator.pushReplacementNamed(context, returnRoute);
  }

  void _update(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "transactionId": transactionId,
      "transactionType": transactionType,
      "transactionTime": transactionTime.toString().substring(0, 23),
      "transactionAmount": transactionAmount,
      "transactionGroupId": transactionGroupId,
      "transactionProjectId": transactionProjectId,
      "transactionPaymentId": transactionPaymentId,
      "transactionAttachment": transactionAttachment,
      "transactionNote": transactionNote,
    };
    final updatedStatus = await widget.dbHelper.updateTransaction(row);
    //print('updated: $updatedStatus');
    Navigator.pushReplacementNamed(context, returnRoute);
  }

  @override
  void initState() {
    super.initState();
    noteController.addListener(() {
      transactionNote = noteController.text.toString();
    });
    amountController.addListener(() {
      if (amountController.text != null && amountController.text != '') {
        transactionAmount = double.parse("" + amountController.text.toString());
      } else {
        transactionAmount = 0.0;
      }
    });
    getCurrencyCode();
  }

  List months = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov',
    'dec'
  ];

  @override
  Widget build(BuildContext context) {
    if (pageShifted) {
      if (ModalRoute.of(context).settings.arguments != null) {
        final Map<String, dynamic> data =
            ModalRoute.of(context).settings.arguments;

        if (data['returnRoute'] != null) {
          returnRoute = data['returnRoute'];
        }

        if (data["returnData"] != null) {
          transactionId = data["returnData"]["transactionId"];
          transactionType = data["returnData"]["transactionType"];
          transactionTime =
              DateTime.parse(data["returnData"]["transactionTime"]);
          transactionAmount = data["returnData"]["transactionAmount"];
          transactionGroupId = data["groupId"] != null
              ? data["groupId"]
              : data["returnData"]["transactionGroupId"];
          transactionProjectId = data["projectId"] != null
              ? data["projectId"]
              : data["returnData"]["transactionProjectId"];
          transactionPaymentId = data["returnData"]["transactionPaymentId"];
          transactionAttachment = data["returnData"]["transactionAttachment"];
          transactionNote = data["returnData"]["transactionNote"];
          returnRoute = data["returnData"]['returnRoute'];
          amountController.text =
              transactionAmount != null ? transactionAmount.toString() : '';
          noteController.text = transactionNote;
          pageShifted = false;

        } else if (data["transactionId"] != null) {
          transactionId = data["transactionId"].toString();
          transactionType = data["transactionType"];
          transactionTime = DateTime.parse(data["transactionTime"]);
          transactionAmount = data["transactionAmount"];
          transactionGroupId = data["transactionGroupId"];
          transactionProjectId = data["transactionProjectId"];
          transactionPaymentId = data["transactionPaymentId"];
          transactionAttachment = data["transactionAttachment"];
          transactionNote = data["transactionNote"];

          amountController.text = transactionAmount.toString();
          noteController.text = transactionNote;

        } else {
          transactionType = data["transactionType"];
          transactionProjectId = data["transactionProjectId"];
          pageShifted = false;
        }
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, returnRoute);
                },
                child: new Text(
                  "Cancel",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Spacer(),
              InkWell(
                child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width / 4,
                    margin: EdgeInsets.only(right: 4),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border.all(width: 2.0, color: Colors.grey),
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: <Widget>[
                        Text(
                          '${months[transactionTime.month - 1]}',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.red,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          '${transactionTime.day}',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                onTap: () async {
                  DateTime tempDate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime.utc(1800, 1, 1),
                    lastDate: DateTime.now().add(Duration(days: 30)),
                  );

                  setState(() {
                    transactionTime =
                        tempDate == null ? DateTime.now() : tempDate;
                  });
                },
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    if (transactionId != null) {
                      _update(context);
                    } else {
                      _insert(context);
                    }
                  }
                },
                child: new Text(
                  "Save",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * 0.9,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
            child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4 - 8,
                          height: MediaQuery.of(context).size.height * 0.4,
                          margin: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(width: 2.0, color: Colors.grey),
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                          ),
                          child: Center(
/*                             child: _image == null
                                ? IconButton(
                                    icon: Icon(Icons.attach_file),
                                    onPressed: () {
                                      getImage();
                                    },
                                    iconSize: 100,
                                  )
                                : InkWell(
                                    child: Image.file(_image),
                                    onTap: () {
                                      getImage();
                                    },
                                  ), */

                            child: transactionId != null && pageShifted
                                ? FutureBuilder<List<Map<String, dynamic>>>(
                                    future: widget.dbHelper.getTransaction(
                                        int.parse(transactionId)),
                                    builder: (context,
                                        AsyncSnapshot<
                                                List<Map<String, dynamic>>>
                                            dataList) {
                                      if (dataList.hasData) {
                                        pageShifted = false;

                                        transactionAttachment = dataList.data[0]
                                            ['transactionAttachment'];

                                        return transactionAttachment != null
                                            ? InkWell(
                                                child: Image.memory(
                                                    base64Decode(
                                                        transactionAttachment)),
                                                onTap: () {
                                                  getImage();
                                                },
                                              )
                                            : IconButton(
                                                icon: Icon(Icons.attach_file),
                                                onPressed: () {
                                                  getImage();
                                                },
                                                iconSize: 100,
                                              );
                                      } else {
                                        return IconButton(
                                          icon: Icon(Icons.attach_file),
                                          onPressed: () {
                                            getImage();
                                          },
                                          iconSize: 100,
                                        );
                                      }
                                    })
                                : transactionAttachment == null
                                    ? IconButton(
                                        icon: Icon(Icons.attach_file),
                                        onPressed: () {
                                          getImage();
                                        },
                                        iconSize: 100,
                                      )
                                    : InkWell(
                                        child: Image.memory(base64Decode(
                                            transactionAttachment)),
                                        onTap: () {
                                          getImage();
                                        },
                                      ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4 - 8,
                          height: MediaQuery.of(context).size.height * 0.25,
                          margin: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(width: 2.0, color: Colors.grey),
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Spacer(),
                              Flexible(
                                child: Text(
                                  currencyCode,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 40,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
/*                               Text('rate', textAlign: TextAlign.center),
                              Text('120', textAlign: TextAlign.center), */
                              Spacer(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6 - 10,
                            height: MediaQuery.of(context).size.height * 0.25,
                            padding: EdgeInsets.only(right: 10),
                            color: Colors.grey[400],
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6 -
                                          10,
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.1,
                                        margin: EdgeInsets.only(right: 5),
                                        child: Text(
                                          '\$',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 40,
                                            color: transactionType == 0
                                                ? Colors.red
                                                : Colors.green,
                                          ),
                                          textAlign: TextAlign.right,
                                        ),
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                    0.5 -
                                                25,
                                        padding: EdgeInsets.only(top: 8),
                                        child: TextFormField(
                                          controller: amountController,
                                          focusNode: amountFocusNode,
                                          autofocus: true,
                                          textAlign: TextAlign.left,
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 40,
                                            color: transactionType == 0
                                                ? Colors.red
                                                : Colors.green,
                                          ),
                                          decoration: InputDecoration(
                                              fillColor: Colors.grey[300],
                                              filled: true,
                                              border: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              errorBorder: InputBorder.none,
                                              disabledBorder: InputBorder.none,
                                              errorStyle: TextStyle(
                                                  color: Colors.red,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 12)),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              amountFocusNode.requestFocus();

                                              return "Enter a valid amount.";
                                            }

                                            try {
                                              transactionAmount =
                                                  double.parse(value);
                                            } catch (exception) {
                                              amountFocusNode.requestFocus();

                                              return "Enter a valid amount.";
                                            }

                                            if (double.parse(value) <= 0) {
                                              amountFocusNode.requestFocus();

                                              return "Enter a valid amount.";
                                            }

                                            return null;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6 -
                                          10,
                                  child: Row(children: <Widget>[
                                    Spacer(),
                                    Flexible(
                                      child: Text(
                                        'Curr. : ' + currencyCode,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.right,
                                      ),
                                    ),
                                  ]),
                                ),
                                Spacer(),
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6 - 8,
                            height: MediaQuery.of(context).size.height * 0.4,
                            padding: EdgeInsets.all(4),
                            child: Column(
                              children: <Widget>[
                                Spacer(),
                                Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  alignment: Alignment.topRight,
                                  color: Colors.grey[300],
                                  child: FutureBuilder<
                                          List<Map<String, dynamic>>>(
                                      future: getGroups(),
                                      builder: (context,
                                          AsyncSnapshot<
                                                  List<Map<String, dynamic>>>
                                              dataList) {
                                        if (dataList.hasData) {
                                          return new DropdownButtonFormField<
                                              int>(
                                            value: transactionGroupId,
                                            isExpanded: true,
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Select a group.';
                                              }
                                              return null;
                                            },
                                            items: dataList.data
                                                .map(
                                                  (item) =>
                                                      new DropdownMenuItem(
                                                          value: int.parse(
                                                              item['groupId']
                                                                  .toString()),
                                                          child: Text(item[
                                                              'groupName'])),
                                                )
                                                .toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                transactionGroupId = value;
                                              });
                                            },
                                            onTap: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context)
                                                  .pushReplacementNamed(
                                                      "/selectlist",
                                                      arguments: <String,
                                                          dynamic>{
                                                    "menu": 0,
                                                    "returnRoute":
                                                        "/insertTransaction",
                                                    "returnData": {
                                                      "transactionId":
                                                          transactionId,
                                                      "transactionType":
                                                          transactionType,
                                                      "transactionTime":
                                                          transactionTime
                                                              .toString()
                                                              .substring(0, 23),
                                                      "transactionAmount":
                                                          transactionAmount,
                                                      "transactionGroupId":
                                                          transactionGroupId,
                                                      "transactionProjectId":
                                                          transactionProjectId,
                                                      "transactionPaymentId":
                                                          transactionPaymentId,
                                                      "transactionAttachment":
                                                          transactionAttachment,
                                                      "transactionNote":
                                                          transactionNote,
                                                      "returnRoute":
                                                          returnRoute,
                                                    }
                                                  });
                                            },
                                          );
                                        } else {
                                          return new DropdownButtonFormField<
                                              int>(
                                            value: 1,
                                            isExpanded: true,
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Setup  group first.';
                                              }
                                              return null;
                                            },
                                            items: [],
                                            onChanged: (value) {},
                                          );
                                        }
                                      }),
                                ),
                                Spacer(),
                                Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  alignment: Alignment.topRight,
                                  color: Colors.grey[300],
                                  child: FutureBuilder<
                                          List<Map<String, dynamic>>>(
                                      future: getProjects(),
                                      builder: (context,
                                          AsyncSnapshot<
                                                  List<Map<String, dynamic>>>
                                              dataList) {
                                        if (dataList.hasData) {
                                          return new DropdownButtonFormField<
                                              int>(
                                            value: transactionProjectId,
                                            isExpanded: true,
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Select a project.';
                                              }
                                              return null;
                                            },
                                            items: dataList.data
                                                .map(
                                                  (item) =>
                                                      new DropdownMenuItem(
                                                          value: int.parse(
                                                              item['projectId']
                                                                  .toString()),
                                                          child: Text(item[
                                                              'projectName'])),
                                                )
                                                .toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                transactionProjectId = value;
                                              });
                                            },
                                            onTap: () {
                                              Navigator.of(context).pop();
                                              Navigator.of(context)
                                                  .pushReplacementNamed(
                                                      "/selectlist",
                                                      arguments: <String,
                                                          dynamic>{
                                                    "menu": 1,
                                                    "returnRoute":
                                                        "/insertTransaction",
                                                    "returnData": {
                                                      "transactionId":
                                                          transactionId,
                                                      "transactionType":
                                                          transactionType,
                                                      "transactionTime":
                                                          transactionTime
                                                              .toString()
                                                              .substring(0, 23),
                                                      "transactionAmount":
                                                          transactionAmount,
                                                      "transactionGroupId":
                                                          transactionGroupId,
                                                      "transactionProjectId":
                                                          transactionProjectId,
                                                      "transactionPaymentId":
                                                          transactionPaymentId,
                                                      "transactionAttachment":
                                                          transactionAttachment,
                                                      "transactionNote":
                                                          transactionNote,
                                                      "returnRoute":
                                                          returnRoute,
                                                    }
                                                  });
                                            },
                                          );
                                        } else {
                                          return new DropdownButtonFormField<
                                              int>(
                                            value: 1,
                                            isExpanded: true,
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Setup project first.';
                                              }
                                              return null;
                                            },
                                            items: [],
                                            onChanged: (value) {},
                                          );
                                        }
                                      }),
                                ),
                                Spacer(),
                                Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  alignment: Alignment.topRight,
                                  color: Colors.grey[300],
                                  child: FutureBuilder<
                                          List<Map<String, dynamic>>>(
                                      future: getPayments(),
                                      builder: (context,
                                          AsyncSnapshot<
                                                  List<Map<String, dynamic>>>
                                              dataList) {
                                        if (dataList.hasData) {
                                          return new DropdownButtonFormField<
                                              int>(
                                            value: transactionPaymentId,
                                            isExpanded: true,
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Select a payment type.';
                                              }
                                              print(value);
                                              return null;
                                            },
                                            items: dataList.data
                                                .map(
                                                  (item) =>
                                                      new DropdownMenuItem(
                                                          value: int.parse(
                                                              item['paymentId']
                                                                  .toString()),
                                                          child: Text(item[
                                                              'paymentName'])),
                                                )
                                                .toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                transactionPaymentId = value;
                                              });
                                            },
                                          );
                                        } else {
                                          return new DropdownButtonFormField<
                                              int>(
                                            value: 1,
                                            isExpanded: true,
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Setup payment type.';
                                              }
                                              return null;
                                            },
                                            items: [],
                                            onChanged: (value) {},
                                          );
                                        }
                                      }),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Container(
                color: Colors.grey,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Notes',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                    transactionNote == null
                        ? SizedBox(
                            height: 0,
                          )
                        : Text(transactionNote),
                    Center(
                      child: IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.7,
                                    child: Row(
                                      children: <Widget>[
                                        Text("Note"),
                                        Spacer(),
                                        FlatButton(
                                          child: Icon(Icons.add),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  content: TextField(
                                      keyboardType: TextInputType.multiline,
                                      controller: noteController,
                                      minLines: 3,
                                      maxLines: null),
                                  actions: [
                                    FlatButton(
                                      child: Text("Close"),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    )
                                  ],
                                );
                              });
                        },
                        iconSize: 40,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        )),
      ),
    );
  }
}
