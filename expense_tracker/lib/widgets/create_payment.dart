import 'dart:convert';
import 'package:expense_tracker/models/db_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconpicker/flutter_iconpicker.dart';

class CreatePayment extends StatefulWidget {
  final dbHelper = DbManager.instance;

  @override
  _CreatePaymentState createState() => _CreatePaymentState();
}

class _CreatePaymentState extends State<CreatePayment> {
  final _formKey = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final accountController = TextEditingController();
  final noteController = TextEditingController();

  bool pageShifted = true;

  String paymentId;
  String paymentName;
  String paymentAccount;
  String paymentNote;
  String paymentIcon;

  String iconDataToJSONString(IconData data) {
    Map<String, dynamic> map = <String, dynamic>{};
    map['codePoint'] = data.codePoint;
    map['fontFamily'] = data.fontFamily;
    map['fontPackage'] = data.fontPackage;
    map['matchTextDirection'] = data.matchTextDirection;
    return jsonEncode(map);
  }

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Icon _icon;

  _pickIcon() async {
    IconData icon = await FlutterIconPicker.showIconPicker(context,
        iconPackMode: IconPack.material);

    paymentIcon = icon != null ? iconDataToJSONString(icon) : null;

    _icon = Icon(
      icon,
      size: 50,
    );
    setState(() {});

    debugPrint('Picked Icon:  $icon');
  }

  void _insert(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "paymentName": paymentName,
      "paymentAccount": paymentAccount,
      "paymentNote": paymentNote,
      "paymentIcon": paymentIcon
    };
    final id = await widget.dbHelper.insertPayment(row);
    //print('inserted row id: $id');
    Navigator.pushReplacementNamed(context, '/tables',
        arguments: <String, dynamic>{
          "menu": 3,
        });
  }

  void _update(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "paymentId": paymentId,
      "paymentName": paymentName,
      "paymentAccount": paymentAccount,
      "paymentNote": paymentNote,
      "paymentIcon": paymentIcon
    };
    final updatedStatus = await widget.dbHelper.updatePayment(row);
    //print('updated: $updatedStatus');
    Navigator.pushReplacementNamed(context, '/tables',
        arguments: <String, dynamic>{
          "menu": 3,
        });
  }

  @override
  Widget build(BuildContext context) {
    if (pageShifted) {
      if (ModalRoute.of(context).settings.arguments != null) {
        final Map<String, dynamic> data =
            ModalRoute.of(context).settings.arguments;

        paymentId = data["paymentId"].toString();
        paymentName = data["paymentName"];
        paymentAccount = data["paymentAccount"];
        paymentNote = data["paymentNote"];
        paymentIcon = data["paymentIcon"];

        nameController.text = paymentName;
        accountController.text = paymentAccount;
        noteController.text = paymentNote;

        if (data["paymentIcon"] != null) {
          _icon = Icon(
            fromJSONStringToIconData(data["paymentIcon"]),
            size: 50,
          );
          setState(() {});
        }
      }

      pageShifted = false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, "/tables",
                      arguments: <String, dynamic>{
                        "menu": 3,
                      });
                },
                child: new Text(
                  "Cancel",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
              Spacer(),
              paymentId == null ? Text("New payment") : Text("Edit payment"),
              Spacer(),
              GestureDetector(
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    if (paymentId != null) {
                      _update(context);
                    } else {
                      _insert(context);
                    }
                  }
                },
                child: new Text(
                  "Save",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width - 10,
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(labelText: 'Payment Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter Payment Name.';
                    } else {
                      paymentName = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  controller: accountController,
                  decoration: InputDecoration(labelText: 'Account'),
                  validator: (value) {
                    paymentAccount = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  controller: noteController,
                  decoration: InputDecoration(labelText: 'Remarks'),
                  validator: (value) {
                    paymentNote = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: _pickIcon,
                    child: Text('Set Icon'),
                  ),
                ),
                SizedBox(height: 10),
                AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: _icon != null ? _icon : Container()),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
