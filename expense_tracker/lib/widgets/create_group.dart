import 'dart:convert';
import 'package:expense_tracker/models/db_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconpicker/flutter_iconpicker.dart';

class CreateGroup extends StatefulWidget {

  final dbHelper = DbManager.instance;

  @override
  _CreateGroupState createState() => _CreateGroupState();
}

class _CreateGroupState extends State<CreateGroup> {
  final _formKey = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final accountController = TextEditingController();
  final noteController = TextEditingController();


  bool pageShifted = true;
  
  String groupId;
  String groupName;
  String groupAccount;
  String groupNote;
  String groupIcon;


  String iconDataToJSONString(IconData data) {
    Map<String, dynamic> map = <String, dynamic>{};
    map['codePoint'] = data.codePoint;
    map['fontFamily'] = data.fontFamily;
    map['fontPackage'] = data.fontPackage;
    map['matchTextDirection'] = data.matchTextDirection;
    return jsonEncode(map);
  }

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Icon _icon;

  _pickIcon() async {

    IconData icon = await FlutterIconPicker.showIconPicker(context,
        iconPackMode: IconPack.material);

    groupIcon = icon != null ? iconDataToJSONString(icon) : null;

    _icon = Icon(icon, size: 50,);
    setState(() {});

    debugPrint('Picked Icon:  $icon');
  }
  
  
  void _insert(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "groupName": groupName,
      "groupAccount": groupAccount,
      "groupNote": groupNote,
      "groupIcon": groupIcon
    };
    final id = await widget.dbHelper.insertGroup(row);
    //print('inserted row id: $id');
    Navigator.pushReplacementNamed(context, '/tables');
  }

  void _update(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "groupId" : groupId,
      "groupName": groupName,
      "groupAccount": groupAccount,
      "groupNote": groupNote,
      "groupIcon": groupIcon
    };
    final updatedStatus = await widget.dbHelper.updateGroup(row);
    //print('updated: $updatedStatus');
    Navigator.pushReplacementNamed(context, '/tables');
  }

  @override
  Widget build(BuildContext context) {

    if(pageShifted){

      if(ModalRoute.of(context).settings.arguments != null){

        final  Map<String, dynamic> data = ModalRoute.of(context).settings.arguments;

        groupId = data["groupId"].toString();
        groupName = data["groupName"];
        groupAccount = data["groupAccount"];
        groupNote = data["groupNote"];
        groupIcon = data["groupIcon"];

        nameController.text = groupName;
        accountController.text = groupAccount;
        noteController.text = groupNote;
        
        if(data["groupIcon"] != null){

          _icon = Icon(fromJSONStringToIconData(data["groupIcon"]), size: 50,);
          setState(() {});

        }
        

      }

      pageShifted = false;

    }

    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, "/tables");
                },
                child: new Text(
                  "Cancel",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
              Spacer(),
              groupId == null ? Text("New Group") : Text("Edit Group"),
              Spacer(),
              GestureDetector(
                onTap: () {
                 if (_formKey.currentState.validate()) {
                       if(groupId != null){

                          _update(context);

                       }else{

                           _insert(context);

                       }

                      }
                },
                child: new Text(
                  "Save",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width - 10,
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(labelText: 'Group Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter Group Name.';
                    } else {
                      groupName = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  controller: accountController,
                  decoration: InputDecoration(labelText: 'Account'),
                  validator: (value) {
                    groupAccount = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  controller: noteController,
                  decoration: InputDecoration(labelText: 'Remarks'),
                  validator: (value) {
                    groupNote = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: _pickIcon,
                    child: Text('Set Icon'),
                  ),
                ),
                SizedBox(height: 10),
                AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: _icon != null ? _icon : Container()),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
