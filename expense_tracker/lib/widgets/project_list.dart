import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/db_manager.dart';

class ProjectList {
  static final dbHelper = DbManager.instance;

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Future<List<Map<String, dynamic>>> _getData({String searchText}) async {
    
    return searchText !=  null ? await dbHelper.getAllProjects(searchText: searchText)
                     : await dbHelper.getAllProjects();
  }

  Future<int> _delete(int id) async {
    final stat = await dbHelper.deleteProject(id);
    return stat;
  }

  Widget getProjectList(BuildContext context,
      {String returnRoute,
      Map<String, dynamic> returnData,
      TextEditingController  searchTextController,
      String searchText}) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.search),
          title: TextFormField(
            controller: searchTextController,

          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.6,
          child: FutureBuilder<List<Map<String, dynamic>>>(
              future: searchText == null ? _getData() : _getData(searchText: searchText),
              builder: (context,
                  AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
                if (dataList.hasData) {
                  return ListView.builder(
                      itemCount: dataList.data.length,
                      itemBuilder: (BuildContext ctx, int index) {
                        if (dataList.data[index]['projectId'] != null) {
                          return Container(
                            decoration: BoxDecoration(
                              border: Border(
                                  bottom: new BorderSide(color: Colors.grey)),
                            ),
                            child: Dismissible(
                              background: Container(
                                color: Colors.red,
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                ),
                              ),
                              key: Key(UniqueKey().toString()),
                              confirmDismiss:
                                  (DismissDirection direction) async {
                                return await showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: const Text("Confirm"),
                                      content: const Text(
                                          "Are you sure you wish to delete this?"),
                                      actions: <Widget>[
                                        FlatButton(
                                            onPressed: () async {
                                              int stat = await _delete(dataList
                                                  .data[index]['projectId']);
                                              if (stat == -1) {
                                                return await showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      title:
                                                          const Text("Error"),
                                                      content: const Text(
                                                          "There are Transactions under this item."),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          onPressed: () =>
                                                              Navigator.of(
                                                                      context)
                                                                  .pop(false),
                                                          child:
                                                              const Text("OK"),
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              } else {
                                                return Navigator.of(context)
                                                    .pop(true);
                                              }
                                            },
                                            child: const Text("DELETE")),
                                        FlatButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(false),
                                          child: const Text("CANCEL"),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              onDismissed: (direction) {
                                _delete(dataList.data[index]['projectId']);
                              },
                              child: ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    border: Border.all(
                                        width: 2.0, color: Colors.grey),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  child: dataList.data[index]['projectIcon'] !=
                                          null
                                      ? Icon(
                                          fromJSONStringToIconData(dataList
                                              .data[index]['projectIcon']),
                                          size: 40,
                                        )
                                      : Icon(
                                          Icons.home,
                                          size: 40,
                                        ),
                                ),
                                title:
                                    Text(dataList.data[index]['projectName']),
                                trailing: IconButton(
                                  icon: Icon(
                                    Icons.arrow_right,
                                    size: 40,
                                  ),
                                  onPressed: () {},
                                ),
                                onTap: () {
                                  if (returnRoute == null) {
                                    Navigator.of(context).pushReplacementNamed(
                                        "/createproject",
                                        arguments: <String, dynamic>{
                                          "projectId": dataList.data[index]
                                              ['projectId'],
                                          "projectName": dataList.data[index]
                                              ['projectName'],
                                          "projectAccount": dataList.data[index]
                                              ['projectAccount'],
                                          "projectNote": dataList.data[index]
                                              ['projectNote'],
                                          "projectIcon": dataList.data[index]
                                              ['projectIcon'],
                                        });
                                  } else {
                                    Navigator.of(context).pushReplacementNamed(
                                        returnRoute,
                                        arguments: <String, dynamic>{
                                          "projectId": dataList.data[index]
                                              ['projectId'],
                                          "projectName": dataList.data[index]
                                              ['projectName'],
                                          "projectAccount": dataList.data[index]
                                              ['projectAccount'],
                                          "projectNote": dataList.data[index]
                                              ['projectNote'],
                                          "projectIcon": dataList.data[index]
                                              ['projectIcon'],
                                          "returnData": returnData
                                        });
                                  }
                                },
                              ),
                            ),
                          );
                        } else {
                          return Center(
                            child: SizedBox(),
                          );
                        }
                      });
                } else {
                  return Center(
                    child: SizedBox(),
                  );
                }
              }),
        ),
      ],
    );
  }
}
