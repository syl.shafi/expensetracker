import 'dart:convert';
import 'package:expense_tracker/models/db_manager.dart';
import 'package:flutter/material.dart';

class CreateCurrency extends StatefulWidget {
  final dbHelper = DbManager.instance;

  @override
  _CreateCurrencyState createState() => _CreateCurrencyState();
}

class _CreateCurrencyState extends State<CreateCurrency> {
  final _formKey = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final codeController = TextEditingController();
  final noteController = TextEditingController();

  bool pageShifted = true;

  String currencyId;
  String currencyName;
  String currencyCode;
  String currencyNote;
  int currencyIsDefault;

  String iconDataToJSONString(IconData data) {
    Map<String, dynamic> map = <String, dynamic>{};
    map['codePoint'] = data.codePoint;
    map['fontFamily'] = data.fontFamily;
    map['fontPackage'] = data.fontPackage;
    map['matchTextDirection'] = data.matchTextDirection;
    return jsonEncode(map);
  }

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  void _insert(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "currencyName": currencyName,
      "currencyCode": currencyCode,
      "currencyNote": currencyNote,
      "currencyIsDefault": currencyIsDefault
    };
    final id = await widget.dbHelper.insertCurrency(row);
    //print('inserted row id: $id');
    Navigator.pushReplacementNamed(context, '/tables',
        arguments: <String, dynamic>{
          "menu": 2,
        });
  }

  void _update(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "currencyId": currencyId,
      "currencyName": currencyName,
      "currencyCode": currencyCode,
      "currencyNote": currencyNote,
      "currencyIsDefault": currencyIsDefault
    };
    final updatedStatus = await widget.dbHelper.updateCurrency(row);
    //print('updated: $updatedStatus');
    Navigator.pushReplacementNamed(context, '/tables',
        arguments: <String, dynamic>{
          "menu": 2,
        });
  }

  @override
  Widget build(BuildContext context) {
    if (pageShifted) {
      if (ModalRoute.of(context).settings.arguments != null) {
        final Map<String, dynamic> data =
            ModalRoute.of(context).settings.arguments;

        currencyId = data["currencyId"].toString();
        currencyName = data["currencyName"];
        currencyCode = data["currencyCode"];
        currencyNote = data["currencyNote"];
        currencyIsDefault = data["currencyIsDefault"];

        nameController.text = currencyName;
        codeController.text = currencyCode;
        noteController.text = currencyNote;
      }

      pageShifted = false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, "/tables",
                      arguments: <String, dynamic>{
                        "menu": 2,
                      });
                },
                child: new Text(
                  "Cancel",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
              Spacer(),
              currencyId == null ? Text("New currency") : Text("Edit currency"),
              Spacer(),
              GestureDetector(
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    if (currencyId != null) {
                      _update(context);
                    } else {
                      _insert(context);
                    }
                  }
                },
                child: new Text(
                  "Save",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width - 10,
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(labelText: 'Currency Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter Currency Name.';
                    } else {
                      currencyName = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  controller: codeController,
                  decoration: InputDecoration(labelText: 'Code'),
                  validator: (value) {
                    currencyCode = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  controller: noteController,
                  decoration: InputDecoration(labelText: 'Remarks'),
                  validator: (value) {
                    currencyNote = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Text("Active: "),
                      SizedBox(width: 4,),
                      Checkbox(
                          value: currencyIsDefault == 1 ? true : false,
                          onChanged: (value) {
                            setState(() {
                              currencyIsDefault = value ? 1 : 0;
                            });
                          })
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
