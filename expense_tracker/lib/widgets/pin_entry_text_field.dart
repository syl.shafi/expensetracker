import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/db_manager.dart';

class PinEntryTextField {
  List<FocusNode> _focusNodes = new List<FocusNode>();
  List<TextEditingController> _textControllers =
      new List<TextEditingController>();
  String currPin;

  void focusNextField(int index) {
    if (index < 3) {
      index++;
    }
    _focusNodes[index].requestFocus();
  }

  Widget buildTextField(int i, BuildContext context) {
    _focusNodes.add(new FocusNode());
    _textControllers.add(new TextEditingController());

    _textControllers[i].addListener(() {
      if (_textControllers[i].text.length > 0) {
        focusNextField(i);
      }
    });

    return Container(
      width: MediaQuery.of(context).size.width / 5,
      margin: EdgeInsets.only(right: 10.0),
      child: TextField(
        controller: _textControllers[i],
        keyboardType: TextInputType.number,
        textAlign: TextAlign.center,
        maxLength: 1,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
          fontSize: 20,
        ),
        focusNode: _focusNodes[i],
        obscureText: false,
        autofocus: i == 0 ? true : false,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            counterText: "",
            border: OutlineInputBorder(borderSide: BorderSide(width: 2.0))),
        onChanged: (String str) {},
        onSubmitted: (String str) async {
          String pin = '';

          _textControllers.forEach((controller) {
            pin += controller.text;
          });

          if (pin.length == 4) {


           final dbHelper = DbManager.instance;

           var rows =  await dbHelper.getPin(pin);

           currPin = rows[0]['pinText'];


            if (currPin != pin) {

                Navigator.of(context).pushReplacementNamed('/', arguments: <String, dynamic>{ 'loggedIn' : false });

            }
            else{

                SharedPreferences dataPrefs = await SharedPreferences.getInstance();

                await dataPrefs.setBool('loggedIn', true);

                Navigator.of(context).pushReplacementNamed('/', arguments: <String, dynamic>{ 'loggedIn' : true });

            }
          }
        },
      ),
    );
  }

  Widget generateTextFields(BuildContext context) {
    List<Widget> textFields = List.generate(4, (int i) {
      return buildTextField(i, context);
    });

    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: textFields);
  }

  Widget getPinEntryTextField(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: generateTextFields(context),
    );
  }
}
