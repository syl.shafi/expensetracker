import 'dart:convert';
import 'package:flutter/material.dart';
import 'bottom_menu.dart';
import '../models/db_manager.dart';

class Dashboard {
  final dbHelper = DbManager.instance;

  String currDate;
  int currTransactionId;
  BottomMenu bottomMenu = new BottomMenu(currentIndex: 0);

  int projectId;
  String projectName;
  String projectIcon;
  double projectBalance;
  bool needReloadBalance = false;

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Future<List<Map<String, dynamic>>> getBalance(String projectId) async {
    List<Map<String, dynamic>> rows =
        await dbHelper.getBalance(projectId: projectId);
    return rows;
  }

  Future<int> _delete(int id) async {
    final rowsDeleted = await dbHelper.deleteTransaction(id);

  }

  void openInsertTransactionScreen(
      BuildContext context, int transactionType, int transactionProjectId) {
    Navigator.of(context).pushReplacementNamed('/insertTransaction',
        arguments: <String, dynamic>{
          "transactionType": transactionType,
          "transactionProjectId": transactionProjectId,
        });
  }

  Widget getDashboard(
      BuildContext context,
      int currProjectId,
      String currProjectName,
      String currProjectIcon,
      double currProjectBalance,
      bool currNeedReloadBalance,
      Function getData,
      String currencyCode,
      Function callSetState) {
    projectId = currProjectId;
    projectName = currProjectName;
    projectIcon = currProjectIcon;
    projectBalance = currProjectBalance;
    needReloadBalance = currNeedReloadBalance;

    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: MediaQuery.of(context).size.width,
          child: needReloadBalance
              ? FutureBuilder<List<Map<String, dynamic>>>(
                  future: getBalance(projectId.toString()),
                  builder: (context,
                      AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
                    if (dataList.hasData) {
                      projectBalance = dataList.data[0]['balance'] == null
                          ? 0
                          : dataList.data[0]['balance'];

                      return Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 4),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border:
                                  Border.all(width: 2.0, color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                            ),
                            child: projectIcon != null
                                ? Icon(
                                    fromJSONStringToIconData(projectIcon),
                                    size: 40,
                                  )
                                : Icon(
                                    Icons.home,
                                    size: 40,
                                  ),
                          ),
                          Flexible(
                            child: Text(
                              projectName,
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Spacer(),
                          Text(
                            '$currencyCode ${projectBalance.toStringAsFixed(2)}',
                            style: TextStyle(fontSize: 14),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.arrow_right,
                              size: 30,
                            ),
                            onPressed: () {
                              Navigator.of(context).pushReplacementNamed(
                                  "/selectlist",
                                  arguments: <String, dynamic>{
                                    "menu": 1,
                                    "returnRoute": "/",
                                    "returnData": {"needReloadBalance": true}
                                  });
                            },
                          ),
                        ],
                      );
                    } else {
                      return Text('Error!!');
                    }
                  },
                )
              : Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 4),
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border.all(width: 2.0, color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      ),
                      child: projectIcon != null
                          ? Icon(
                              fromJSONStringToIconData(projectIcon),
                              size: 40,
                            )
                          : Icon(
                              Icons.home,
                              size: 40,
                            ),
                    ),
                    Flexible(
                      child: Text(
                        projectName,
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Spacer(),
                    Text('$currencyCode ${projectBalance.toStringAsFixed(2)}',
                        style: TextStyle(fontSize: 14)),
                    IconButton(
                      icon: Icon(
                        Icons.arrow_right,
                        size: 30,
                      ),
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed(
                            "/selectlist",
                            arguments: <String, dynamic>{
                              "menu": 1,
                              "returnRoute": "/",
                              "returnData": {"needReloadBalance": true}
                            });
                      },
                    ),
                  ],
                ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                    child: MaterialButton(
                      onPressed: () {
                        openInsertTransactionScreen(context, 1, projectId);
                      },
                      color: Colors.green,
                      textColor: Colors.white,
                      child: Icon(
                        Icons.control_point,
                        size: MediaQuery.of(context).size.width / 5,
                      ),
                      padding: EdgeInsets.all(10),
                      shape: CircleBorder(),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                    child: MaterialButton(
                      onPressed: () {
                        openInsertTransactionScreen(context, 0, projectId);
                      },
                      color: Colors.red,
                      textColor: Colors.white,
                      child: Icon(
                        Icons.remove_circle_outline,
                        size: MediaQuery.of(context).size.width / 5,
                      ),
                      padding: EdgeInsets.all(10),
                      shape: CircleBorder(),
                    ),
                  ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.7,
                child: FutureBuilder<List<Map<String, dynamic>>>(
                    future: getData(projectId.toString(), 100),
                    builder: (context,
                        AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
                      if (dataList.hasData) {
                        return ListView.builder(
                          itemCount: dataList.data.length,
                          itemBuilder: (BuildContext ctx, int index) {
                            if (currDate == null ||
                                currDate !=
                                    dataList.data[index]['transactionTime']
                                        .toString()
                                        .substring(0, 10) ||
                                currTransactionId ==
                                    dataList.data[index]['transactionId']) {
                              currTransactionId =
                                  dataList.data[index]['transactionId'];

                              currDate = dataList.data[index]['transactionTime']
                                  .toString()
                                  .substring(0, 10);

                              return Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                      color: Colors.grey,
                                    ),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      currDate,
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.right,
                                    ),
                                  ),
                                  Dismissible(
                                      background: Container(
                                        color: Colors.red,
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.white,
                                        ),
                                      ),
                                      key: Key(UniqueKey().toString()),
                                      confirmDismiss:
                                          (DismissDirection direction) async {
                                        return await showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: const Text("Confirm"),
                                              content: const Text(
                                                  "Are you sure you wish to delete this?"),
                                              actions: <Widget>[
                                                FlatButton(
                                                    onPressed: () =>
                                                        Navigator.of(context)
                                                            .pop(true),
                                                    child:
                                                        const Text("DELETE")),
                                                FlatButton(
                                                  onPressed: () =>
                                                      Navigator.of(context)
                                                          .pop(false),
                                                  child: const Text("CANCEL"),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                      onDismissed: (direction) async {
                                       await _delete(
                                            dataList.data[index]
                                                ['transactionId']);
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                              bottom: new BorderSide(
                                                  color: Colors.grey)),
                                        ),
                                        child: ListTile(
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 20.0, vertical: 10.0),
                                          leading: Container(
                                            decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              border: Border.all(
                                                  width: 2.0,
                                                  color: Colors.grey),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8.0)),
                                            ),
                                            child: dataList.data[index]
                                                        ['groupIcon'] !=
                                                    null
                                                ? Icon(
                                                    fromJSONStringToIconData(
                                                        dataList.data[index]
                                                            ['groupIcon']),
                                                    size: 40,
                                                  )
                                                : Icon(
                                                    Icons.poll,
                                                    size: 40,
                                                  ),
                                          ),
                                          title: Text(dataList.data[index]
                                              ['groupName']),
                                          subtitle: dataList.data[index]
                                                      ['transactionType'] !=
                                                  0
                                              ? Text(
                                                  '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.green),
                                                )
                                              : Text(
                                                  '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                                  style: TextStyle(
                                                      color: Colors.red)),
                                          trailing: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                3,
                                            child: IconButton(
                                              icon: Icon(
                                                Icons.arrow_right,
                                                size: 40,
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                          onTap: () {
                                            Navigator.of(context)
                                                .pushReplacementNamed(
                                                    '/insertTransaction',
                                                    arguments: <String,
                                                        dynamic>{
                                                  "transactionId":
                                                      dataList.data[index]
                                                          ['transactionId'],
                                                  "transactionType":
                                                      dataList.data[index]
                                                          ['transactionType'],
                                                  "transactionTime":
                                                      dataList.data[index]
                                                          ['transactionTime'],
                                                  "transactionAmount":
                                                      dataList.data[index]
                                                          ['transactionAmount'],
                                                  "transactionGroupId": dataList
                                                          .data[index]
                                                      ['transactionGroupId'],
                                                  "transactionProjectId": dataList
                                                          .data[index]
                                                      ['transactionProjectId'],
                                                  "transactionPaymentId": dataList
                                                          .data[index]
                                                      ['transactionPaymentId'],
                                                  "transactionAttachment": dataList
                                                          .data[index]
                                                      ['transactionAttachment'],
                                                  "transactionNote":
                                                      dataList.data[index]
                                                          ['transactionNote'],
                                                });
                                          },
                                        ),
                                      )),
                                  dataList.data.length == index + 1
                                      ? ListTile()
                                      : SizedBox()
                                ],
                              );
                            } else {
                              return Column(children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                        bottom:
                                            new BorderSide(color: Colors.grey)),
                                  ),
                                  child: Dismissible(
                                    background: Container(
                                      color: Colors.red,
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.white,
                                      ),
                                    ),
                                    key: Key(UniqueKey().toString()),
                                    confirmDismiss:
                                        (DismissDirection direction) async {
                                      return await showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: const Text("Confirm"),
                                            content: const Text(
                                                "Are you sure you wish to delete this?"),
                                            actions: <Widget>[
                                              FlatButton(
                                                  onPressed: () =>
                                                      Navigator.of(context)
                                                          .pop(true),
                                                  child: const Text("DELETE")),
                                              FlatButton(
                                                onPressed: () =>
                                                    Navigator.of(context)
                                                        .pop(false),
                                                child: const Text("CANCEL"),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                    onDismissed: (direction) async {
                                      await _delete(
                                          dataList.data[index]['transactionId']);
                                    },
                                    child: ListTile(
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 20.0, vertical: 10.0),
                                      leading: Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(
                                              width: 2.0, color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8.0)),
                                        ),
                                        child: dataList.data[index]
                                                    ['groupIcon'] !=
                                                null
                                            ? Icon(
                                                fromJSONStringToIconData(
                                                    dataList.data[index]
                                                        ['groupIcon']),
                                                size: 40,
                                              )
                                            : Icon(
                                                Icons.poll,
                                                size: 40,
                                              ),
                                      ),
                                      title: Text(
                                          dataList.data[index]['groupName']),
                                      subtitle: dataList.data[index]
                                                  ['transactionType'] !=
                                              0
                                          ? Text(
                                              '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                              style: TextStyle(
                                                  color: Colors.green),
                                            )
                                          : Text(
                                              '$currencyCode  ${dataList.data[index]['transactionAmount'].toStringAsFixed(2)}',
                                              style:
                                                  TextStyle(color: Colors.red)),
                                      trailing: Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.arrow_right,
                                            size: 40,
                                          ),
                                          onPressed: () {},
                                        ),
                                      ),
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushReplacementNamed(
                                                '/insertTransaction',
                                                arguments: <String, dynamic>{
                                              "transactionId": dataList
                                                  .data[index]['transactionId'],
                                              "transactionType":
                                                  dataList.data[index]
                                                      ['transactionType'],
                                              "transactionTime":
                                                  dataList.data[index]
                                                      ['transactionTime'],
                                              "transactionAmount":
                                                  dataList.data[index]
                                                      ['transactionAmount'],
                                              "transactionGroupId":
                                                  dataList.data[index]
                                                      ['transactionGroupId'],
                                              "transactionProjectId":
                                                  dataList.data[index]
                                                      ['transactionProjectId'],
                                              "transactionPaymentId":
                                                  dataList.data[index]
                                                      ['transactionPaymentId'],
                                              "transactionAttachment":
                                                  dataList.data[index]
                                                      ['transactionAttachment'],
                                              "transactionNote":
                                                  dataList.data[index]
                                                      ['transactionNote'],
                                            });
                                      },
                                    ),
                                  ),
                                ),
                                dataList.data.length == index + 1
                                    ? ListTile()
                                    : SizedBox()
                              ]);
                            }
                          },
                        );
                      } else {
                        return CircularProgressIndicator();
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomMenu.getBottomMenu(context),
    );
  }
}
