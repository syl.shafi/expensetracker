import 'package:flutter/material.dart';
import 'bottom_menu.dart';
import 'group_list.dart';
import 'project_list.dart';
import 'currency_list.dart';
import 'payment_list.dart';

class SelectList extends StatefulWidget {
  
  final GroupList groupList = new GroupList();
  final ProjectList projectList = new ProjectList();
  final CurrencyList currencyList = new CurrencyList();
  final PaymentList paymentList = new PaymentList();

  @override
  _SelectListState createState() => _SelectListState();
}

class _SelectListState extends State<SelectList> {
  BottomMenu bottomMenu = new BottomMenu(currentIndex: 2);

  String searchText = '';
  TextEditingController searchTextController = new TextEditingController();

  void changeSearchText(String text){
        
        setState(() {
          searchText = text;
        });
         
  }

  void callSetState(){
        
        setState(() {
        });
         
  }


  bool pageShifted = true;

  int menu = 0;
  String returnRoute = "";
  Map<String, dynamic> returnData = {};
  

   @override
  void initState() {
    super.initState();
    searchTextController.addListener(() {
      changeSearchText(searchTextController.text);
    });
  }

  Widget build(BuildContext context) {

    if(pageShifted){

      if(ModalRoute.of(context).settings.arguments != null){

        final  Map<String, dynamic> data = ModalRoute.of(context).settings.arguments;
        menu = int.parse(data["menu"].toString());
        returnRoute = data["returnRoute"].toString();
        returnData = data["returnData"];

      }

      pageShifted = false;

    }

    return Scaffold(
      appBar: AppBar(
         title: Text(menu == 0 ? "Select Group" : menu == 1 ? "Select Project" : menu == 2 ? "Select Currency": "Select Payment"),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.8,
                width: MediaQuery.of(context).size.width,
                child: menu == 0
                    ? widget.groupList.getGroupList(context, returnRoute : returnRoute, returnData: returnData, searchTextController: searchTextController, searchText: searchText)
                    : menu == 1 ? widget.projectList.getProjectList(context, returnRoute : returnRoute, returnData: returnData, searchTextController: searchTextController, searchText: searchText)
                    : menu == 2 ? widget.currencyList.getCurrencyList(context, callSetState)
                    : widget.paymentList.getPaymentList(context),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomMenu.getBottomMenu(context),
    );
  }
}
