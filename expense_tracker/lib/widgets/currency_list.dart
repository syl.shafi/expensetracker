import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/db_manager.dart';

class CurrencyList {
  static final dbHelper = DbManager.instance;

  IconData fromJSONStringToIconData(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    return IconData(
      map['codePoint'],
      fontFamily: map['fontFamily'],
      fontPackage: map['fontPackage'],
      matchTextDirection: map['matchTextDirection'],
    );
  }

  Future<List<Map<String, dynamic>>> _getData() async {
    return await dbHelper.getAllCurrencies();
  }

  Future<int> _delete(int id) async {
    final rowsDeleted = await dbHelper.deleteCurrency(id);
    //print('deleted $rowsDeleted row(s): row $id');
    return rowsDeleted;
  }

  Widget getCurrencyList(BuildContext context, Function callSetState) {
    return FutureBuilder<List<Map<String, dynamic>>>(
        future: _getData(),
        builder: (context, AsyncSnapshot<List<Map<String, dynamic>>> dataList) {
          if (dataList.hasData) {
            return ListView.builder(
                itemCount: dataList.data.length,
                itemBuilder: (BuildContext ctx, int index) {
                  if (dataList.data[index]['currencyId'] != null) {
                    return Container(
                      decoration: BoxDecoration(
                        border:
                            Border(bottom: new BorderSide(color: Colors.grey)),
                      ),
                      child: Dismissible(
                        background: Container(
                          color: Colors.red,
                          child: Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                        ),
                        key: Key(UniqueKey().toString()),
                        confirmDismiss: (DismissDirection direction) async {
                          return await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text("Confirm"),
                                content: const Text(
                                    "Are you sure you wish to delete this?"),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(true),
                                      child: const Text("DELETE")),
                                  FlatButton(
                                    onPressed: () =>
                                        Navigator.of(context).pop(false),
                                    child: const Text("CANCEL"),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                        onDismissed: (direction) async {

                          await _delete(dataList.data[index]['currencyId']);

                          callSetState();

                        },
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10.0),
                          leading: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border:
                                  Border.all(width: 2.0, color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                            ),
                            child: dataList.data[index]['currencyIsDefault'] == 1
                                ? Icon(
                                    Icons.monetization_on,
                                    size: 40,
                                  )
                                : Icon(
                                    Icons.attach_money,
                                    size: 40,
                                  ),
                          ),
                          title: Text(dataList.data[index]['currencyName']),
                          trailing: IconButton(
                            icon: Icon(
                              Icons.arrow_right,
                              size: 40,
                            ),
                            onPressed: () {},
                          ),
                          subtitle: Text(dataList.data[index]['currencyCode']),
                          onTap: () {
                            Navigator.of(context).pushReplacementNamed(
                                "/createcurrency",
                                arguments: <String, dynamic>{
                                  "currencyId": dataList.data[index]['currencyId'],
                                  "currencyName": dataList.data[index]
                                      ['currencyName'],
                                  "currencyCode": dataList.data[index]
                                      ['currencyCode'],
                                  "currencyNote": dataList.data[index]
                                      ['currencyNote'],
                                  "currencyIsDefault": dataList.data[index]
                                      ['currencyIsDefault'],
                                });
                          },
                        ),
                      ),
                    );
                  } else {
                    return Center(
                      child: SizedBox(),
                    );
                  }
                });
          } else {
            return Center(
              child: SizedBox(),
            );
          }
        });
  }
}
