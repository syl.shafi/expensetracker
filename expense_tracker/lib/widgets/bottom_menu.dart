import 'package:flutter/material.dart';

class BottomMenu {

 final int currentIndex;

 BottomMenu({this.currentIndex});


  Widget getBottomMenu(BuildContext context) {

    return BottomNavigationBar(
      currentIndex: currentIndex,
      selectedItemColor: Colors.amber[800],
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.star),
          title: Text('Main'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.history),
          title: Text('History'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.list),
          title: Text('Tables'),
        ),        
/*         BottomNavigationBarItem(
          icon: Icon(Icons.send),
          title: Text('Send'),
        ), */
        BottomNavigationBarItem(
          icon: Icon(Icons.more_horiz),
          title: Text('More'),
        ),
      ],
      onTap: (int index){
        if (index == 0){
            Navigator.of(context).pushReplacementNamed("/");
        }
        if (index == 1){
            Navigator.of(context).pushReplacementNamed("/history");
        }
        if (index == 2){
            Navigator.of(context).pushReplacementNamed("/tables");
        }
        if (index == 3){
            Navigator.of(context).pushReplacementNamed("/more");
        }
      },
    );
  }
}
