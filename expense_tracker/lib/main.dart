import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './widgets/pin_entry_text_field.dart';
import 'widgets/dashboard.dart';
import './widgets/insert_transaction.dart';
import 'widgets/tables.dart';
import 'widgets/create_group.dart';
import 'widgets/create_project.dart';
import 'widgets/create_currency.dart';
import 'widgets/create_payment.dart';
import 'widgets/select_list.dart';
import 'widgets/history.dart';
import 'widgets/more.dart';
import './models/db_manager.dart';

void main() {
  SharedPreferences.setMockInitialValues({});
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Expense Tracker',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: const Color(0xFFEFEFEF),
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/insertTransaction': (context) => InsertTransaction(),
        '/tables': (context) => Tables(),
        '/creategroup': (context) => CreateGroup(),
        '/createproject': (context) => CreateProject(),
        '/createcurrency': (context) => CreateCurrency(),
        '/createpayment': (context) => CreatePayment(),
        '/selectlist': (context) => SelectList(),
        '/history': (context) => History(),
        '/more': (context) => More(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  final dbHelper = DbManager.instance;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PinEntryTextField pinEntryTextField = new PinEntryTextField();
  Dashboard dashboard = new Dashboard();

  Future<List<Map<String, dynamic>>> getData(
      String projectId, int limit) async {
    List<Map<String, dynamic>> rows = await widget.dbHelper
        .getOptimizedTransactionList(
            projectId: projectId, limit: limit.toString());

    //await widget.dbHelper.deleteTransaction(rows[rows.length-1]['transactionId']);

    return rows;
  }

  Future<List<Map<String, dynamic>>> getCurrentProject() async {
    List<Map<String, dynamic>> rows = await widget.dbHelper.getCurrentProject();
    return rows;
  }

  Future<void> getCurrencyCode() async {
    List<Map<String, dynamic>> rows = await widget.dbHelper.getCurrency();

      if (rows.length == 0) {
        currencyCode = 'US\$';
      }else{

        currencyCode = rows[0]['currencyCode'];

      }
    
  }

  void callSetState(){
    setState(() {
      
    });
  }

  int projectId;
  String projectName;
  String projectIcon;
  double projectBalance;
  bool needReloadBalance = false;
  bool isPageShifted = true;
  bool loggedIn = true;
  String loginError;
  String currencyCode;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrencyCode();
    getCurrentProject();
  }

  @override
  Widget build(BuildContext context) {
    if (isPageShifted) {
      final Map<String, dynamic> data =
          ModalRoute.of(context).settings.arguments;
      if (data != null) {
        if (data["loggedIn"] != null) {
          loggedIn = data["loggedIn"];

          if (!loggedIn) {
            loginError = 'Incorrect PIN.';
          }
        } else {
          projectId = int.parse(data["projectId"].toString());
          projectName = data["projectName"].toString();
          projectIcon = data["projectIcon"];
          projectBalance = null;

          needReloadBalance = data['returnData']["needReloadBalance"];

          isPageShifted = false;
        }

        setState(() {});
      }
    }

    return FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (context, AsyncSnapshot<SharedPreferences> dataPrefs) {
          if (dataPrefs.hasData) {
            loggedIn = dataPrefs.data.getBool('loggedIn') == null ||
                    dataPrefs.data.getBool('loggedIn') == false
                ? false
                : true;

            return !loggedIn
                ? Scaffold(
                    appBar: AppBar(
                      title: Text("Passcode"),
                      centerTitle: true,
                    ),
                    body: Center(
                      child: Container(
                        height: MediaQuery.of(context).size.height / 4,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Enter Your PIN",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                            loginError != null
                                ? SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    height: 20,
                                  )
                                : SizedBox(),
                            loginError != null
                                ? Text(
                                    loginError,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )
                                : SizedBox(),
                            SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 20,
                            ),
                            pinEntryTextField.getPinEntryTextField(context),
                          ],
                        ),
                      ),
                    ))
                : projectId == null
                    ? FutureBuilder<List<Map<String, dynamic>>>(
                        future: getCurrentProject(),
                        builder: (context,
                            AsyncSnapshot<List<Map<String, dynamic>>>
                                dataList) {
                          if (dataList.hasData) {
                            projectId = dataList.data[0]['projectId'];
                            projectName = dataList.data[0]['projectName'];
                            projectIcon = dataList.data[0]['projectIcon'];
                            projectBalance =
                                dataList.data[0]['projectBalance'] == null
                                    ? 0
                                    : dataList.data[0]['projectBalance'];

                            return dashboard.getDashboard(
                                context,
                                projectId,
                                projectName,
                                projectIcon,
                                projectBalance,
                                needReloadBalance,
                                getData,
                                currencyCode,
                                callSetState);
                          } else {
                            return Scaffold(
                              appBar: AppBar(
                                title: Text('Loading....'),
                              ),
                              body: CircularProgressIndicator(),
                            );
                          }
                        })
                    : dashboard.getDashboard(
                        context,
                        projectId,
                        projectName,
                        projectIcon,
                        projectBalance,
                        needReloadBalance,
                        getData,
                        currencyCode,
                        callSetState);
          } else {
            return Scaffold(
                appBar: AppBar(
                  title: Text("Passcode"),
                  centerTitle: true,
                ),
                body: Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height / 4,
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Enter Your PIN",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        loginError != null
                            ? SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: 20,
                              )
                            : SizedBox(),
                        loginError != null
                            ? Text(
                                loginError,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              )
                            : SizedBox(),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 20,
                        ),
                        pinEntryTextField.getPinEntryTextField(context),
                      ],
                    ),
                  ),
                ));
          }
        });
  }
}
